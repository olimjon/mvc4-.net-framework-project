﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _00002390_DBSD_CW2.Controllers
{
    public class OrderController : Controller
    {

        public ActionResult Create()
        {
            Order o = new Order();
            Product p = (Product)Session["product"];
            o.Product.PType.TypeOfProduct = p.PType.TypeOfProduct;
            o.Product.Theme = p.Theme;
            o.Product.Height = p.Height;
            o.Product.Width = p.Width;
            string quantityError = (string)TempData["quantity_error"];
            string maxOrderError = (string)TempData["max_order_error"];
            ModelState.AddModelError("",quantityError);
            ModelState.AddModelError("", maxOrderError);
            return View("Order", o);
        }

        //
        // POST: /Order/Create

        [HttpPost]
        public ActionResult Create(Order o)
        {
            try
            {
                Product p = (Product)Session["product"];
                if (o.Quantity <= p.Quantity)
                {
                    string uname = this.User.Identity.Name;
                    o.Product.ProductNo = p.ProductNo;
                    Client client = DbRepository.GetClientByUsername(uname);
                    o.Client.ClientNo = client.ClientNo;
                    DbRepository.InsertOrder(o);
                    // TODO: Add insert logic here
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    TempData["quantity_error"] = string.Format("You cannot order more than {0} units of this product", p.Quantity);
                    return RedirectToAction("Create");
                }
            }
            catch (SqlException)
            {
                TempData["max_order_error"] ="You cannot order on more than 5 000 000";
                return RedirectToAction("Create");
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Login", "Account");
            }
            catch
            {
                return View("Order", o);
            }
        }

      
    }
}
