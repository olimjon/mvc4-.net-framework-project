﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _00002390_DBSD_CW2.Controllers
{
    public class ChangePasswordController : Controller
    {
       
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View("ChangePassword");
        }

        //
        // POST: /ChangePassword/Create

        [HttpPost]
        public ActionResult ChangePassword(Client c, ChangePasswordModel cp)
        {
            try
            {

                string uname = this.User.Identity.Name;
                c = DbRepository.GetClientByUsername(uname);
                if (cp.OldPassword == c.Password)
                {
                    if (cp.NewPassword == cp.OldPassword)
                    {
                        ModelState.AddModelError("", "New password and old password couldn't be the same");
                        return View();
                    }
                    else
                    {

                        c.Password = cp.NewPassword;
                        DbRepository.UpdateClient(c);
                        return RedirectToAction("Login", "Account");
                    }
                }
                 
                else
                {
                    ModelState.AddModelError("", "Incorrect old password");
                    return View();
                }
            }
            catch (SqlException)
            {
                ModelState.AddModelError("", "Unable to change password during holidays and weekends");
                return View();
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}
