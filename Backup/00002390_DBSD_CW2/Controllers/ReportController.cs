﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _00002390_DBSD_CW2.Controllers
{
    public class ReportController : Controller
    {
       
        //
        // GET: /Report/Create
        [Authorize]
        public ActionResult Report()
        {
            List<ReportRecord> list = new List<ReportRecord>();
            Report model = new Report();
            model.Records = list;
            return View(model);
        }

        //
        // POST: /Report/Create

        [HttpPost]
        public ActionResult Report(Report form)
        {
            try
            {
             form.Records = DbRepository.GetReport(form);
                // TODO: Add insert logic here
                return View(form);
                //return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
