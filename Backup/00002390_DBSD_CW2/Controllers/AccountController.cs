﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _00002390_DBSD_CW2.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
            return View();
        }

        // POST: /Login/Create

        [HttpPost]
        public ActionResult Login(UserModel user, string returnUrl)
        {
                int authenticated = DbRepository.Login(user.UserName,user.Password);
                if (authenticated>0)
                {
                    FormsAuthentication.SetAuthCookie(user.UserName, false);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && 
                           returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Product");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Authentication failed!");
                }
          
            return View(user);

        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        
    }
}
