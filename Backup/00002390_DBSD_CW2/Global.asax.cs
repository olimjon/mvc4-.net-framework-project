﻿using _00002390_DBSD_CW2.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace _00002390_DBSD_CW2
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            string connectionString = WebConfigurationManager
            .ConnectionStrings["00002390CW2Db"]
            .ConnectionString;

            string createDbConnectionString = WebConfigurationManager
                  .ConnectionStrings["CreateDbConnectionString"]
                  .ConnectionString;
            DbInitializer.InitDB(createDbConnectionString, connectionString);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}