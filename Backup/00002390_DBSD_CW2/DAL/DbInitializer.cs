﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;

namespace _00002390_DBSD_CW2.DAL
{
    public class DbInitializer
    {
        public static void InitDB(string createDbConnectionString, string connectionString)
        {
            //create database  (use NpgsqlConnection for Postgres)
            using (DbConnection con = new SqlConnection(createDbConnectionString))
            {
                using (DbCommand cmd = con.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "create database [00002390]";
                        con.Open();
                        cmd.ExecuteNonQuery();

                    }
                    catch
                    {

                        return;
                    
                    }
                    
                        
                   
                }
            }
            //execute script (use NpgsqlConnection for Postgres)
            string sql = File.ReadAllText(HostingEnvironment.MapPath(@"~/00002390_DBSD_CW2_DDL.sql"));
            using (DbConnection con = new SqlConnection(connectionString))
            {
                using (DbCommand cmd = con.CreateCommand())
                {
                    var scripts = Regex.Split(sql, @"(\s+|;)GO(\s+|;)", RegexOptions.Multiline);
                    foreach (var splitScript in scripts)
                    {
                        cmd.CommandText = splitScript;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }
    }
}