﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class Order
    {
        public Order()
        {
            Product = new Product();
            Secretary = new Staff();
            Client = new Client();
        }
        public int OrderNo { get; set; }
        public string OrderStatus { get; set; }
        [Range(1, 1000)]
        public int Quantity { get; set; }
        public DateTime OrderDate { get; set; }
        public virtual Product Product { get; set; }
        public virtual Staff Secretary { get; set; }
        public virtual Client Client { get; set; }
    }
}