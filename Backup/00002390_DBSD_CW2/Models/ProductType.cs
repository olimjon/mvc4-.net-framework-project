﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class ProductType
    {
        public string TypeOfProduct { get; set; }
        public int PricePerSqCm { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}