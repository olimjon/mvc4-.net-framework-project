﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class Product
    {
        public Product()
        {
            PType = new ProductType();
        }
        public int ProductNo { get; set; }
        public string Theme { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public int Quantity { get; set; }
        [Display(Name = "Required Time")]
        public int TimeToMake { get; set; }
        public double Price { get; set; }
        [Display(Name = "Type")]
        public virtual ProductType PType { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}