﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
namespace _00002390_DBSD_CW2.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        [Authorize]
        public ActionResult Index(string sortOrder, string currentFilter, string searchByType, string searchByTheme, string searchByTime, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ThemeSortParm = String.IsNullOrEmpty(sortOrder) ? "theme_desc" : "";
            ViewBag.TypeSortParm = sortOrder == "Type" ? "type_desc" : "Type";
            ViewBag.WidthSortParm = sortOrder == "Width" ? "width_desc" : "Width";
            ViewBag.HeightSortParm = sortOrder == "Height" ? "height_desc" : "Height";
            ViewBag.QuantitySortParm = sortOrder == "Quantity" ? "quantity_desc" : "Quantity";
            ViewBag.TimeSortParm = sortOrder == "Time" ? "time_desc" : "Time";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "price_desc" : "Price";
            if (searchByTheme != null)
            {

                page = 1;
            }
            else
            {
                searchByTheme = currentFilter;
            }
            ViewBag.CurrentFilter = searchByTheme;
            
            var products = from p in DbRepository.GetAllProducts() select p;

            ViewBag.TypeParam = searchByType;
            ViewBag.TimeParam = searchByTime;

            if (!string.IsNullOrEmpty(searchByTime))
            {
                products = products.Where(p => p.TimeToMake.ToString() == searchByTime); //|| p.PType.TypeOfProduct.ToUpper().StartsWith(searchString.ToUpper()) || p.Theme.ToUpper().StartsWith(searchString.ToUpper())).ToList();

            }
            if (!string.IsNullOrEmpty(searchByType))
            {
                products = products.Where(p => p.PType.TypeOfProduct.ToUpper().StartsWith(searchByType.ToUpper()));

            }
            if (!string.IsNullOrEmpty(searchByTheme))
            {
                products = products.Where(p => p.Theme.ToUpper().StartsWith(searchByTheme.ToUpper()));

            }
            
            
            //var pagedList = products.ToPagedList(pageNumber, _pageSize);
           //IPagedList<Product> productsToReturn = null;

            switch (sortOrder)
            {
                case "theme_desc":
                    products = products.OrderByDescending(p => p.Theme);
                    break;
                case "Type":
                    products = products.OrderBy(p => p.PType.TypeOfProduct);
                    break;
                case "type_desc":
                    products = products.OrderByDescending(p => p.PType.TypeOfProduct);
                    break;
                case "Width":
                    products = products.OrderBy(p => p.Width);
                    break;
                case "width_desc":
                    products = products.OrderByDescending(p => p.Width);
                    break;
                case "Height":
                    products= products.OrderBy(p => p.Height);
                    break;
                case "height_desc":
                    products = products.OrderByDescending(p => p.Height);
                    break;
                case "Quantity":
                    products = products.OrderBy(p => p.Quantity);
                    break;
                case "quantity_desc":
                    products= products.OrderByDescending(p => p.Quantity);
                    break;
                case "Time":
                    products = products.OrderBy(p => p.TimeToMake);
                    break;
                case "time_desc":
                    products = products.OrderByDescending(p => p.TimeToMake);
                    break;
                case "Price":
                    products = products.OrderBy(p => p.Price);
                    break;
                case "price_desc":
                    products = products.OrderByDescending(p => p.Price);
                    break;
                default:
                    products = products.OrderBy(p => p.Theme);
                    break;
            }
            int _pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(products.ToPagedList(pageNumber, _pageSize));
        }
        



        //
        // GET: /Product/Order/5

        public ActionResult Order(int id)
        {
            Product p = DbRepository.GetProductById(id);
            Session["product"] = p;
            return RedirectToAction("Create", "Order");
        }
    }
}
