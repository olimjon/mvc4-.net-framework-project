﻿using _00002390_DBSD_CW2.DAL;
using _00002390_DBSD_CW2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _00002390_DBSD_CW2.Controllers
{
    public class ClientController : Controller
    {
        
        ////
        // GET: /Client/Details/5
        [Authorize]
        public ActionResult Details(string uname)
        {
            try
            {
                uname = this.User.Identity.Name;
                Client c = DbRepository.GetClientByUsername(uname);
                return View(c);
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }

        }

        ////
        //// GET: /Client/Create

        public ActionResult Registration()
        {
            return View();
        }

        //
        // POST: /Client/Create

        [HttpPost]
        public ActionResult Registration(Client c)
        {
            try
            {
                DbRepository.InsertClient(c);
                return RedirectToAction("Login", "Account");
            }
            catch (SqlException ex)
            {
                if (ex.Message.ToLower().Contains("clients_dob_ck"))
                {
                    ModelState.AddModelError("", "Invalid date of birth");
                    return View();
                }
                else
                {
                    ModelState.AddModelError("", "Phone is not in the rigth format! please follow this format +xxx-xx-xxx-xx-xx");
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

       
    }
}
