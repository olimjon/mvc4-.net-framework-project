﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class Staff
    {
        public int StaffNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }
        public DateTime HireDate { get; set; }
    }
}