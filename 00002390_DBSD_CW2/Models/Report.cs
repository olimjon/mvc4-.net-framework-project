﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class Report
    {
        
        public int Month { get; set; }
        public string Type { get; set; }
        [Display(Name = "Minimal Revenue")]
        public int minVal { get; set; }
        public List<ReportRecord> Records { get; set; }

    }
}