﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _00002390_DBSD_CW2.Models
{
    public class ReportRecord
    {
        public int ClientNo { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Number of Orders")]
        public int NumberOfOrders { get; set; }
        public double Revenue { get; set; }
        [Display(Name = "Total Quantity")]
        public int TotalQuantity { get; set; }
    }
}