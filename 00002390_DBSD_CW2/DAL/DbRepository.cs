﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;
using _00002390_DBSD_CW2.Models;
using Dapper;


namespace _00002390_DBSD_CW2.DAL
{
    public class DbRepository
    {
        
        public static string ConnectionStr
        {
            get
            {
                return WebConfigurationManager
                    .ConnectionStrings["00002390CW2Db"]
                    .ConnectionString;
            }
        }

        public static int Login(string username, string password)
        {
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "stp_login";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    DbParameter pUsername = cmd.CreateParameter();
                    pUsername.Direction = ParameterDirection.Input;
                    pUsername.ParameterName = "@uname";
                    pUsername.DbType = System.Data.DbType.String;
                    pUsername.Value = username;
                    cmd.Parameters.Add(pUsername);

                    DbParameter pPassword = cmd.CreateParameter();
                    pPassword.Direction = ParameterDirection.Input;
                    pPassword.ParameterName = "@password";
                    pPassword.DbType = System.Data.DbType.String;
                    pPassword.Value = password;
                    cmd.Parameters.Add(pPassword);

                    DbParameter pResult = cmd.CreateParameter();
                    pResult.Direction = ParameterDirection.Output;
                    pResult.ParameterName = "@loggedin";
                    pResult.DbType = DbType.Int32;
                    cmd.Parameters.Add(pResult);

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    return (int)pResult.Value;
                }
            }
        }

        public static void InsertClient(Client c)
        {
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                conn.Open();
                conn.Execute(@"stp_insert_client",
                    new
                    {
                        title = c.PersonTitle,
                        fname = c.FirstName,
                        lname = c.LastName,
                        email = c.Email,
                        phone = c.Phone,
                        country = c.Country,
                        city = c.City,
                        dob = c.DoB,
                        username = c.Username,
                        password = c.Password
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static List<Product> GetAllProducts()
        {
            List<Product> list = new List<Product>();
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT PRODUCTNO, PTYPE , HEIGHT, WIDTH, THEME, QUANTITY, TIMETOMAKE, PRICE FROM PRODUCT";

                    conn.Open();

                    using (DbDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Product p = new Product();
                            p.ProductNo = rdr.GetInt32(rdr.GetOrdinal("productno"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("ptype")))
                                p.PType.TypeOfProduct = rdr.GetString(rdr.GetOrdinal("ptype"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("height")))
                                p.Height = rdr.GetDouble(rdr.GetOrdinal("height"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("width")))
                                p.Width = rdr.GetDouble(rdr.GetOrdinal("width"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("theme")))
                                p.Theme = rdr.GetString(rdr.GetOrdinal("theme"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("quantity")))
                                p.Quantity = rdr.GetInt32(rdr.GetOrdinal("quantity"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("timetomake")))
                                p.TimeToMake = rdr.GetInt32(rdr.GetOrdinal("timetomake"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("price")))
                                p.Price = rdr.GetDouble(rdr.GetOrdinal("price"));
                            list.Add(p);
                        }
                    }
                }

            }
            return list;
        }

        public static Client GetClientByUsername(string uname)
        {

            Client client = null;
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT CLIENTNO, TITLE,FNAME,LNAME,EMAIL,PHONE,COUNTRY,CITY,DOB,USERNAME,USERPASSWORD
                                       FROM CLIENTS WHERE USERNAME=@username";
                    DbParameter pUsername = cmd.CreateParameter();
                    pUsername.ParameterName = "@username";
                    pUsername.DbType = System.Data.DbType.String;
                    pUsername.Value = uname;
                    cmd.Parameters.Add(pUsername);

                    conn.Open();
                    using (DbDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Client c = new Client();
                            c.ClientNo = (int)rdr
                              .GetInt32(rdr.GetOrdinal("clientno"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("title")))
                                c.PersonTitle = rdr.GetString(rdr.GetOrdinal("title"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("fname")))
                                c.FirstName = rdr.GetString(rdr.GetOrdinal("fname"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("lname")))
                                c.LastName = rdr.GetString(rdr.GetOrdinal("lname"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("email")))
                                c.Email = rdr.GetString(rdr.GetOrdinal("email"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("phone")))
                                c.Phone = rdr.GetString(rdr.GetOrdinal("phone"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("country")))
                                c.Country = rdr.GetString(rdr.GetOrdinal("country"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("city")))
                                c.City = rdr.GetString(rdr.GetOrdinal("city"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("dob")))
                                c.DoB = rdr.GetDateTime(8);

                            if (!rdr.IsDBNull(rdr.GetOrdinal("username")))
                                c.Username = rdr.GetString(rdr.GetOrdinal("username"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("userpassword")))
                                c.Password = rdr.GetString(rdr.GetOrdinal("userpassword"));


                            client = c;
                        }
                    }
                }
            }
            return client;
        }

        public static Product GetProductById(int prodId)
        {
            Product product = null;
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"select productno, ptype, height, width, theme, quantity,timetomake,
                                        price from product where productno=@productno";
                    DbParameter pProductNo = cmd.CreateParameter();
                    pProductNo.ParameterName = "@productno";
                    pProductNo.DbType = System.Data.DbType.Int32;
                    pProductNo.Value = prodId;
                    cmd.Parameters.Add(pProductNo);

                    conn.Open();

                    using (DbDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Product p = new Product();
                            p.ProductNo = rdr.GetInt32(rdr.GetOrdinal("productno"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("ptype")))
                                p.PType.TypeOfProduct = rdr.GetString(rdr.GetOrdinal("ptype"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("height")))
                                p.Height = rdr.GetDouble(rdr.GetOrdinal("height"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("width")))
                                p.Width = rdr.GetDouble(rdr.GetOrdinal("width"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("theme")))
                                p.Theme = rdr.GetString(rdr.GetOrdinal("theme"));
                            if (!rdr.IsDBNull(rdr.GetOrdinal("quantity")))
                                p.Quantity = rdr.GetInt32(rdr.GetOrdinal("quantity"));
                            if (!rdr.IsDBNull(rdr.GetOrdinal("timetomake")))
                                p.TimeToMake = rdr.GetInt32(rdr.GetOrdinal("timetomake"));
                            if (!rdr.IsDBNull(rdr.GetOrdinal("price")))
                                p.Price = rdr.GetDouble(rdr.GetOrdinal("price"));
                            product = p;
                        }
                    }
                }
            }
            return product;
        }

        
        public static void InsertOrder(Order o)
        {
            using(DbConnection conn = new SqlConnection(ConnectionStr))
            {
             conn.Open();
             conn.Execute(@"stp_insert_order",
                        new
                        {
                            product = o.Product.ProductNo,
                            client = o.Client.ClientNo,
                            quantity=o.Quantity
                        }, commandType:CommandType.StoredProcedure);
            }
        }
   

        public static void UpdateClient(Client c)
        {
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                conn.Open();
                conn.Execute(@"
                        UPDATE clients
                        SET userpassword = @password
                        WHERE clientno = @clientno",
                        new
                        {
                            password = c.Password,
                            clientno = (int)c.ClientNo
                        });
            }
        }

        public static List<ReportRecord> GetReport(Report rp)
        {
            List<ReportRecord> list=new List<ReportRecord>();
            using (DbConnection conn = new SqlConnection(ConnectionStr))
            {
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"stp_generate_report";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    DbParameter pMonth = cmd.CreateParameter();
                    pMonth.ParameterName = "@month";
                    pMonth.DbType = System.Data.DbType.Int32;
                    pMonth.Value = rp.Month;
                    cmd.Parameters.Add(pMonth);

                    DbParameter pType = cmd.CreateParameter();
                    pType.ParameterName = "@product";
                    pType.DbType = System.Data.DbType.String;
                    pType.Value = rp.Type;
                    cmd.Parameters.Add(pType);

                    DbParameter pMinRevenue = cmd.CreateParameter();
                    pMinRevenue.ParameterName = "@minVal";
                    pMinRevenue.DbType = System.Data.DbType.String;
                    pMinRevenue.Value = rp.minVal;
                    cmd.Parameters.Add(pMinRevenue);

                    conn.Open();

                    using (DbDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            ReportRecord r = new ReportRecord();
                            if (!rdr.IsDBNull(rdr.GetOrdinal("fname")))
                                r.FirstName = rdr.GetString(rdr.GetOrdinal("fname"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("lname")))
                                r.LastName = rdr.GetString(rdr.GetOrdinal("lname"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("numberOforders")))
                                r.NumberOfOrders = rdr.GetInt32(rdr.GetOrdinal("numberOforders"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("revenue")))
                                r.Revenue = rdr.GetDouble(rdr.GetOrdinal("revenue"));

                            if (!rdr.IsDBNull(rdr.GetOrdinal("totalQuantity")))
                                r.TotalQuantity = rdr.GetInt32(rdr.GetOrdinal("totalQuantity"));

                            list.Add(r);
                        }
                    }
                }
            }
            return list;
        }
    }
}