CREATE SEQUENCE seq_dept_deptno
START WITH 10
INCREMENT BY 10 

CREATE TABLE DEPT(
DEPTNO INT DEFAULT (NEXT VALUE FOR seq_dept_deptno),
DNAME VARCHAR(20) NOT NULL UNIQUE,
CONSTRAINT dept_deptno_pk PRIMARY KEY(deptno)
)

CREATE SEQUENCE seq_clients_clientno
START WITH 2345
INCREMENT BY 1 

/*CLIENTS*/
CREATE TABLE CLIENTS(
CLIENTNO INT DEFAULT (NEXT VALUE FOR seq_clients_clientno),
CONSTRAINT clients_clientno_pk PRIMARY KEY(clientNO), 
TITLE VARCHAR(3) NOT NULL,
FNAME VARCHAR(14) NOT NULL, 
LNAME VARCHAR(14) NOT NULL, 
EMAIL VARCHAR(40) UNIQUE,
PHONE VARCHAR(17) UNIQUE,
COUNTRY VARCHAR(20) NOT NULL,
CITY VARCHAR(20) NOT NULL,
DOB DATE NOT NULL,
USERNAME VARCHAR(20) NOT NULL UNIQUE,
USERPASSWORD VARCHAR(20) NOT NULL,
CONSTRAINT clients_dob_ck CHECK(DATEADD(YEAR, 18, DOB)<=GETDATE() AND DATEADD(YEAR, 100, DOB)>=GETDATE()),
CONSTRAINT clients_userpassword_ck CHECK(LEN(USERPASSWORD)>=6),
CONSTRAINT clients_phone_ck CHECK(PHONE LIKE '+[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'))

/*Staff*/
CREATE TABLE JOB (
POSITION VARCHAR(30),
CONSTRAINT job_position_pk PRIMARY KEY(position),
DEPTNO INT NOT NULL,
CONSTRAINT job_deptno_fk FOREIGN KEY(deptNo) REFERENCES DEPT(deptNo),
SALARY DECIMAL(9,2) NOT NULL,
CONSTRAINT job_salary_ck CHECK(SALARY>=300000) 
)

CREATE SEQUENCE seq_staff_staffno
START WITH 1234
INCREMENT BY 1 

CREATE TABLE STAFF(
STAFFNO INT DEFAULT (NEXT VALUE FOR seq_staff_staffno),
FNAME VARCHAR(14) NOT NULL,
LNAME VARCHAR(14) NOT NULL,
DOB DATE NOT NULL,
PHONE VARCHAR(17) UNIQUE NOT NULL,
POSITION VARCHAR(30) NOT NULL, 
HIREDATE DATE NOT NULL,
CONSTRAINT staff_position_fk FOREIGN KEY(position) REFERENCES JOB(position),
CONSTRAINT staff_staffno_pk PRIMARY KEY(staffno)
)

/*Product*/
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
CREATE TABLE TypeOfProduct (
PTYPE VARCHAR(20),
CONSTRAINT typeofproduct_ptype_pk PRIMARY KEY(ptype),
PricePerSqCm INT, 
CONSTRAINT typeofproduct_pricepersqcm_ck CHECK(pricePerSqCm BETWEEN 8 AND 15) 
)

CREATE SEQUENCE seq_product_productno
START WITH 6543
INCREMENT BY 1 

CREATE TABLE PRODUCT(
PRODUCTNO INT DEFAULT (NEXT VALUE FOR seq_product_productno),
CONSTRAINT product_productno_pk PRIMARY KEY(productNo),
DESIGNERNO INT NOT NULL, 
CONSTRAINT product_designerno_fk FOREIGN KEY(designerNo) REFERENCES STAFF(staffNo),
PTYPE VARCHAR(20) NOT NULL,
CONSTRAINT product_ptype_fk FOREIGN KEY(pType) REFERENCES TypeOfProduct(pType),
HEIGHT FLOAT NOT NULL,
WIDTH FLOAT NOT NULL,
THEME VARCHAR(30) NOT NULL,
QUANTITY INT NOT NULL,
TIMETOMAKE INT NOT NULL,
PRICE FLOAT NOT NULL,
CONSTRAINT product_height_ck CHECK(height BETWEEN 29.7 AND 300),
CONSTRAINT product_width_ck CHECK(width BETWEEN 42 AND 600),
)

/*Orders*/

CREATE SEQUENCE seq_orders_orderno
START WITH 4321
INCREMENT BY 1 ;

CREATE TABLE ORDERS(
ORDERNO INT DEFAULT (NEXT VALUE FOR seq_orders_orderno),
CONSTRAINT orders_orderno_pk PRIMARY KEY(orderNo),
PRODUCTNO INT NOT NULL,
CONSTRAINT orders_productno_fk FOREIGN KEY(productNo) REFERENCES PRODUCT(productNo),
SECRETARYNO INT NOT NULL,
CONSTRAINT orders_secretaryno_fk FOREIGN KEY(secretaryNo) REFERENCES STAFF(staffNo),
CLIENTNO INT NOT NULL,
CONSTRAINT orders_clientno_fk FOREIGN KEY(clientNo) REFERENCES CLIENTS(clientNo),
ORDEREDQUANTITY INT NOT NULL,
ORDERSTATUS VARCHAR(11) NOT NULL, 
ORDERDATE DATE NOT NULL
)

/*Audit table client*/

CREATE TABLE CLIENTS_LOG(
CLIENTNO BIGINT, 
TITLE VARCHAR(3),
FNAME VARCHAR(14), 
LNAME VARCHAR(14), 
EMAIL VARCHAR(40),
PHONE VARCHAR(17),
COUNTRY VARCHAR(20),
CITY VARCHAR(20),
DOB DATE,
USERNAME VARCHAR(20),
USERPASSWORD VARCHAR(20),
OPDATE DATE,
OPERATION VARCHAR(10),
[USER] VARCHAR(100)
)


/*Index*/

/*This index is created to optimize query in stp_login procedure since 
both columns are used together in where clause*/
CREATE INDEX clients_uname_pwd_idx ON CLIENTS(USERNAME, USERPASSWORD)

/*This index is created to optimize query in stp_insert_order since the column
 is used in where clause with equality operator*/
CREATE INDEX orders_status_idx ON ORDERS(ORDERSTATUS)

/*This index is created to optimize queries in stp_generate_report and 
stp_insert_product since in both cases column is used in where clause with equality operator*/
CREATE INDEX product_type_idx ON PRODUCT(PTYPE)

/*This index is created on foreign key column in table orders to speed up
 join operation with table product in query used in stp_generate_report */
CREATE INDEX orders_productno_idx ON ORDERS(PRODUCTNO)

/*This index is created on foreign key column in table orders to speed up
 join operation with table clients in query used in stp_generate_report */
CREATE INDEX orders_clientno_idx ON ORDERS(CLIENTNO)

/*This index is created on column date to optimize query in stp_generate_report 
since the column is used in where clause with equality operator */
CREATE INDEX orders_date_idx ON ORDERS(ORDERDATE)
GO

/*Stored procedures and triggers*/
CREATE PROCEDURE stp_insert_client(
@title varchar(3), @fname varchar(14), @lname varchar(14), @email varchar(40), 
@phone varchar(17), @country varchar(20), @city varchar(20), @dob date, @username varchar(20), 
@password varchar(20)
) AS
BEGIN 
INSERT INTO CLIENTS(TITLE,FNAME,LNAME,EMAIL,PHONE,COUNTRY,CITY,DOB,USERNAME,USERPASSWORD)
VALUES(@title, @fname, @lname, @email, @phone, @country, @city, @dob, @username, @password )
END
GO

CREATE PROCEDURE stp_insert_product(@designer int, @type varchar(20), @height float,
									 @width float,  @theme varchar(30), @quantity int, 
									 @time int)
AS
BEGIN
INSERT INTO PRODUCT(DESIGNERNO, PTYPE, HEIGHT, WIDTH, THEME, QUANTITY, TIMETOMAKE, PRICE)
VALUES (@designer, @type, @height, @width,  @theme, @quantity, @time, (SELECT @width*@height*PricePerSqCm FROM TypeOfProduct tp WHERE PTYPE=@type) )
END
GO

CREATE PROCEDURE stp_login(@uname varchar(20), @password varchar(20),@loggedin bit = 0 OUTPUT)
AS
BEGIN
IF EXISTS(SELECT * FROM CLIENTS WHERE USERNAME = @uname AND USERPASSWORD = @password)
SET @loggedin=1
ELSE 
SET @loggedin=0
END
GO

CREATE PROCEDURE stp_insert_order(@product INT,@client INT, @quantity INT)
AS
BEGIN
DECLARE @secretary int
SET @secretary= (SELECT TOP 1  s.STAFFNO
				FROM STAFF s JOIN ORDERS o ON s.STAFFNO=o.SECRETARYNO
				WHERE o.ORDERSTATUS='IN PROGRESS' 
				group by s.staffno 
				HAVING COUNT(o.ORDERNO)<10)
INSERT INTO ORDERS(PRODUCTNO, SECRETARYNO, CLIENTNO,ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE)
VALUES(@product, @secretary, @client, @quantity, 'IN PROGRESS', GETDATE())
END
GO

/*This report shows names of clients total number of orders made by each of them, total revenue
 generated by client per month, and total quantity of products ordered. The report is built based 
 on user's input. The user have to select the month, the type of the product and optionally might 
 filter results specifying minimum value for revenue. Using information generated by the report the company
 can evaluate the demand on its products,  differentiate which type of product are ordered more
 and which type of products is in less demand, see which customer made the biggest number of orders or generated
 the greatest revenue. 
 
 To optimize query performance indexes were used. To speed up join operation indexes for productno and clientno
 foreign keys were created in orders table. Additionally indexes were created for ptype and orderdate columns since 
 they are used in where clause with equality operators. To reduce performance cost subqueries were avoided and only necessary
 columns were put in the select statement*/
CREATE PROCEDURE stp_generate_report(@month int, @product varchar(20), @minVal bigint)
AS
BEGIN
SELECT c.fname, c.lname, COUNT(o.orderno) AS 'numberOfOrders' , SUM(p.price*o.orderedQuantity) AS 'revenue', SUM(o.orderedQuantity) AS 'totalQuantity'
FROM (ORDERS o JOIN PRODUCT p ON p.productNo=o.productNo)JOIN CLIENTS c ON c.clientNo=o.clientNo 
WHERE  DATEPART(MONTH, o.orderdate) =@month
AND p.pType=@product
GROUP BY c.clientNo, c.fname, c.lname
HAVING SUM(p.price*o.orderedQuantity)>=@minVal
END
GO

CREATE TRIGGER tr_clients_check_holiday ON CLIENTS
AFTER UPDATE
AS DECLARE @isallowed bit =0
BEGIN
IF EXISTS (SELECT 1 FROM INSERTED) AND EXISTS (SELECT 1 FROM DELETED)
BEGIN 
	IF DATEPART(WEEKDAY, GETDATE()) BETWEEN 2 AND 6 
		BEGIN 
			IF (DATEPART(MONTH, GETDATE())!=1 AND DATEPART(DAY, GETDATE())!=1)
			OR (DATEPART(MONTH, GETDATE())!=3 AND DATEPART(DAY, GETDATE())!=8)
			OR (DATEPART(MONTH, GETDATE())!=3 AND DATEPART(DAY, GETDATE())!=21)
			OR (DATEPART(MONTH, GETDATE())!=9 AND DATEPART(DAY, GETDATE())!=1)
			SET @isallowed=1
		END	
	IF @isallowed=0
		BEGIN
			RAISERROR (N'Operation not allowed!', 
					  10, 1 )
			ROLLBACK TRANSACTION
		END
	END
END
GO

CREATE TRIGGER tr_clients_audit on CLIENTS
AFTER INSERT, UPDATE, DELETE
AS DECLARE @upd INT =0
SELECT @upd= COUNT(*) FROM INSERTED
IF (EXISTS (SELECT 1 FROM INSERTED) AND EXISTS (SELECT 1 FROM DELETED)) OR EXISTS (SELECT 1 FROM DELETED)
INSERT INTO CLIENTS_LOG (CLIENTNO, TITLE,FNAME,LNAME,EMAIL,PHONE,COUNTRY,CITY,DOB,USERNAME,USERPASSWORD, OPDATE, OPERATION, [USER])
SELECT CLIENTNO, TITLE,FNAME,LNAME,EMAIL,PHONE,COUNTRY,CITY,DOB,USERNAME,USERPASSWORD, GETDATE()
,  (CASE WHEN @upd>0 THEN 'UPDATE' ELSE 'DELETE' END)
, user   
FROM DELETED
ELSE
INSERT INTO CLIENTS_LOG (CLIENTNO, TITLE,FNAME,LNAME,EMAIL,PHONE,COUNTRY,CITY,DOB,USERNAME,USERPASSWORD, OPDATE, OPERATION, [USER])
SELECT CLIENTNO, TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD, GETDATE()
,  'INSERT'
, user FROM INSERTED
GO


CREATE TRIGGER tr_max_order ON ORDERS
AFTER INSERT
AS
DECLARE 
@isallowed bit=0,
@total int = 0
SET @total=(SELECT p.price*i.orderedQuantity 
			FROM INSERTED i JOIN PRODUCT p ON i.productno=p.productno)
BEGIN
if @total>5000000
	SET @isallowed=1
if @isallowed>0
	BEGIN
		RAISERROR (N'Operation not allowed!', 
					  10, 1 )
		ROLLBACK TRANSACTION
	END
END
GO

/*Department*/

INSERT INTO DEPT(dname) VALUES (
'ACCOUNTING'
)
INSERT INTO DEPT(dname) VALUES (
'PROCUREMENT'
)
INSERT INTO DEPT(dname) VALUES (
'ASSEMBLING'
)
INSERT INTO DEPT(dname) VALUES (
'OPERATION'
)
INSERT INTO DEPT(dname) VALUES (
'DESIGN'
)
/*Clients*/
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Fred', 'Carpenter', 'fcarpenter0@tinyurl.com', '+998-90-123-45-67', 'UZBEKISTAN', 'TASHKENT', '21-Feb-1975', 'fcarpenter0', 'QRLc6WzWF');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Heather', 'Watkins', 'hwatkins1@hugedomains.com', '+998-90-123-45-66', 'UZBEKISTAN', 'TASHKENT', '14-Dec-1982', 'hwatkins1', 'eJvTSc3');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Dorothy', 'Garza', 'dgarza2@smugmug.com', '+998-90-123-45-65', 'UZBEKISTAN', 'TASHKENT', '27-Aug-1970', 'dgarza2', 'wMvSTfbzAPd');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Dorothy', 'Bennett', 'dbennett3@bravesites.com', '+998-90-123-45-64', 'UZBEKISTAN', 'TASHKENT', '10-Sep-1969', 'dbennett3', 'Ah9fEiPBK0n');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Shirley', 'Howard', 'showard4@nifty.com', '+998-90-123-45-63', 'UZBEKISTAN', 'TASHKENT', '08-Oct-1986', 'showard4', 'iSbBV40v');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Debra', 'Day', 'dday5@symantec.com', '+998-90-123-45-61', 'UZBEKISTAN', 'TASHKENT', '11-Aug-1971', 'dday5', '5STH4BszYk');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Barbara', 'Reed', 'breed6@posterous.com', '+998-90-123-45-60', 'UZBEKISTAN', 'TASHKENT', '08-Mar-1975', 'breed6', 'zb7vf5c');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Daniel', 'Smith', 'dsmith7@google.fr', '+998-90-123-45-69', 'UZBEKISTAN', 'TASHKENT', '14-Jun-1989', 'dsmith7', 'B6iGDN5zK1z');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Debra', 'Vasquez', 'dvasquez8@ebay.co.uk', '+998-90-123-45-68', 'UZBEKISTAN', 'TASHKENT', '27-Aug-1984', 'dvasquez8', 'WcVxkxQDw7A');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Dennis', 'Gutierrez', 'dgutierrez9@shinystat.com', '+998-90-123-45-97', 'UZBEKISTAN', 'TASHKENT', '04-Mar-1977', 'dgutierrez9', 'Lzm5pG');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Marilyn', 'Ramirez', 'mramireza@cbc.ca', '+998-90-123-45-87', 'UZBEKISTAN', 'TASHKENT', '22-Dec-1967', 'mramireza', 'x5rKxnxDPY');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Jimmy', 'Hernandez', 'jhernandezb@dion.ne.jp', '+998-90-123-45-77', 'UZBEKISTAN', 'TASHKENT', '30-Aug-1979', 'jhernandezb', 'OnsYF7');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Elizabeth', 'Daniels', 'edanielsc@simplemachines.org', '+998-90-123-15-67', 'UZBEKISTAN', 'TASHKENT', '08-Dec-1971', 'edanielsc', 'QB1soiYgW3s');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Walter', 'George', 'wgeorged@webnode.com', '+998-90-123-45-57', 'UZBEKISTAN', 'TASHKENT', '08-Dec-1982', 'wgeorged', '9TLQNH8b');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Patrick', 'Spencer', 'pspencere@tripod.com', '+998-90-123-45-47', 'UZBEKISTAN', 'TASHKENT', '21-Apr-1987', 'pspencere', 'x0Qe7vTQ');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Johnny', 'Peters', 'jpetersf@businessweek.com', '+998-90-123-45-37', 'UZBEKISTAN', 'TASHKENT', '20-Oct-1967', 'jpetersf', 'JMcj0D');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Timothy', 'Mason', 'tmasong@sakura.ne.jp', '+998-90-123-45-17', 'UZBEKISTAN', 'TASHKENT', '15-Sep-1969', 'tmasong', 'x3t0tyNySg');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Sean', 'James', 'sjamesh@ocn.ne.jp', '+998-90-123-49-67', 'UZBEKISTAN', 'TASHKENT', '14-Sep-1988', 'sjamesh', 'clUpw4um47k');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Carl', 'Crawford', 'ccrawfordi@t-online.de', '+998-90-123-48-67', 'UZBEKISTAN', 'TASHKENT', '17-Nov-1990', 'ccrawfordi', 'qbdj2hAkCK');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Anthony', 'Sanchez', 'asanchezj@hibu.com', '+998-90-123-47-67', 'UZBEKISTAN', 'TASHKENT', '17-Nov-1990', 'asanchezj', 'BR4Cqxo8LMG5');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Ernest', 'Thomas', 'ethomask@about.me', '+998-90-123-46-67', 'UZBEKISTAN', 'TASHKENT', '15-May-1983', 'ethomask', 'JDIif5RKRO');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mr', 'Russell', 'Turner', 'rturnerl@shareasale.com', '+998-90-123-44-67', 'UZBEKISTAN', 'TASHKENT', '15-Mar-1972', 'rturnerl', 'DkRKOQEGI');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Katherine', 'Little', 'klittlem@discovery.com', '+998-90-123-43-67', 'UZBEKISTAN', 'TASHKENT', '25-Dec-1980', 'klittlem', '9FLFhvKgWd');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Mrs', 'Cynthia', 'Martinez', 'cmartinezn@slideshare.net', '+998-90-123-42-67', 'UZBEKISTAN', 'TASHKENT', '03-Jul-1970', 'cmartinezn', 'AX4AxUlvM');
insert into CLIENTS (TITLE, FNAME, LNAME, EMAIL, PHONE, COUNTRY, CITY, DOB, USERNAME, USERPASSWORD) values ('Ms', 'Marilyn', 'Armstrong', 'marmstrongo@msn.com', '+998-90-123-41-67', 'UZBEKISTAN', 'TASHKENT', '26-May-1994', 'marmstrongo', '4M4rhJlYwhe');

INSERT INTO JOB VALUES(
'CEO', 10, 1200000
)
INSERT INTO JOB VALUES(
'ACCOUNTANT', 10, 900000 
)
INSERT INTO JOB VALUES(
'SECRETARY', 40, 500000 
)
INSERT INTO JOB VALUES(
'SYSTEM ADMINISTRATOR', 40, 600000  
)
INSERT INTO JOB VALUES(
'PRINTER', 40, 300000  
)
INSERT INTO JOB VALUES(
'ASSEMBLER', 30, 500000 
)
INSERT INTO JOB VALUES(
'PROCUREMENT SPECIALIST', 20, 600000
)
INSERT INTO JOB VALUES(
'DESIGNER', 50, 600000 
)

INSERT INTO TypeOfProduct VALUES(
'POSTER', 8
)
INSERT INTO TypeOfProduct VALUES(
'BANNER', 10
)
INSERT INTO TypeOfProduct VALUES(
'BILLBOARD', 15
)

/*STAFF*/
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Anthony', 'Cook', '28-Apr-1982', '+998-90-959-40-14', 'DESIGNER', '7/15/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Kathy', 'Martin', '14-Aug-1986', '+998-90-980-58-49', 'DESIGNER', '12/20/2013');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Adam', 'Morris', '28-Aug-1972', '+998-90-961-50-45', 'SECRETARY', '11/27/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Theresa', 'Perry', '23-Dec-1984', '+998-90-959-00-91', 'DESIGNER', '7/26/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Stephen', 'Price', '22-Dec-1983', '+998-90-981-59-07', 'SECRETARY', '8/3/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Carlos', 'Reynolds', '28-Feb-1978', '+998-90-973-93-28', 'CEO', '01/01/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Janet', 'Richards', '19-Sep-1984', '+998-90-987-83-78', 'PROCUREMENT SPECIALIST', '7/12/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Sean', 'Carroll', '25-May-1973', '+998-90-978-03-17', 'PRINTER', '8/1/2013');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Linda', 'Jackson', '09-Feb-1978', '+998-90-976-79-63', 'SECRETARY', '11/5/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Rose', 'Gibson', '12-May-1988', '+998-90-963-17-95', 'ASSEMBLER', '7/17/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Jason', 'Franklin', '09-May-1976', '+998-90-970-90-99', 'ACCOUNTANT', '10/10/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Dorothy', 'Cook', '09-Feb-1982', '+998-90-979-98-09', 'DESIGNER', '6/3/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Gary', 'Ferguson', '05-Dec-1987', '+998-97-975-46-17', 'SECRETARY', '11/21/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Benjamin', 'Thompson', '05-Sep-1972', '+998-97-977-04-53', 'PRINTER', '8/16/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Sara', 'Davis', '25-Nov-1989', '+998-97-968-19-79', 'SECRETARY', '5/18/2013');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Diane', 'Morrison', '06-Aug-1989', '+998-97-757-67-47', 'DESIGNER', '9/27/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Edward', 'Morrison', '06-Jun-1986', '+998-97-770-98-37', 'DESIGNER', '5/17/2011');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Patrick', 'Gonzales', '02-Feb-1986', '+998-97-775-89-79', 'DESIGNER', '3/23/2013');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Marilyn', 'Hicks', '16-May-1977', '+998-97-761-63-85', 'SECRETARY', '10/31/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Wanda', 'Olson', '25-Feb-1981', '+998-97-776-49-61', 'ASSEMBLER', '4/13/2014');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Johnny', 'Henry', '26-Feb-1982', '+998-97-753-43-10', 'SECRETARY', '8/27/2013');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Steven', 'Cooper', '19-Apr-1981', '+998-97-787-86-05', 'SECRETARY', '6/11/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Gary', 'Rogers', '28-Aug-1976', '+998-97-763-44-19', 'ACCOUNTANT', '2/10/2014');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Helen', 'Wright', '16-Apr-1984', '+998-93-761-03-71', 'SECRETARY', '5/6/2012');
insert into STAFF (FNAME, LNAME, DOB, PHONE, POSITION, HIREDATE) values ('Katherine', 'Burton', '10-Jul-1976', '+998-93-764-13-88', 'PROCUREMENT SPECIALIST', '12/12/2011');


/*Product*/
exec stp_insert_product'1237', 'POSTER', '30', '70', 'NAVRUZ', 5, 1
exec stp_insert_product'1251', 'BANNER', '45', '100', 'SALE', 6, 5
exec stp_insert_product'1250', 'BILLBOARD', '30', '200', 'SALE', 5, 15
exec stp_insert_product'1250', 'BILLBOARD', '35', '250', 'NEW YEAR', 6, 17
exec stp_insert_product'1235', 'POSTER', '140', '50', 'SALE', 6, 15
exec stp_insert_product'1249', 'POSTER', '110', '300', 'SALE', 5, 3
exec stp_insert_product'1249', 'POSTER', '300', '250', 'WOMEN''S DAY', 6, 18
exec stp_insert_product'1249', 'POSTER', '140', '50', 'OTHER', 10, 14
exec stp_insert_product'1237', 'BILLBOARD', '140', '400', 'WOMEN''S DAY', 10, 17
exec stp_insert_product'1251', 'BILLBOARD', '100', '320', 'NAVRUZ', 6, 15
exec stp_insert_product'1235', 'BANNER', '200', '320', 'WOMEN''S DAY', 10, 12
exec stp_insert_product'1251', 'POSTER', '60', '50', 'NEW YEAR', 8, 9
exec stp_insert_product'1235', 'POSTER', '60', '140', 'NAVRUZ', 3, 10
exec stp_insert_product'1237', 'POSTER', '35', '500', 'TEACHER''S DAY', 5, 18
exec stp_insert_product'1251', 'BILLBOARD', '42', '45', 'TEACHER''S DAY', 7, 16
exec stp_insert_product'1245', 'POSTER', '110', '45', 'NEW YEAR', 9, 3
exec stp_insert_product'1245', 'BILLBOARD', '200', '300', 'NEW YEAR', 6, 10
exec stp_insert_product'1235', 'BILLBOARD', '140', '400', 'TEACHER''S DAY', 8, 12
exec stp_insert_product'1249', 'POSTER', '35', '350', 'WOMEN''S DAY', 7, 5
exec stp_insert_product'1235', 'BILLBOARD', '30', '70', 'NAVRUZ', 5, 1
exec stp_insert_product'1234', 'BANNER', '45', '45', 'OTHER', 3, 1
exec stp_insert_product'1245', 'BILLBOARD', '140', '50', 'TEACHER''S DAY', 5, 7
exec stp_insert_product'1245', 'BANNER', '70', '320', 'SALE', 4, 10
exec stp_insert_product'1251', 'POSTER', '35', '42', 'TEACHER''S DAY', 9, 18
exec stp_insert_product'1235', 'BILLBOARD', '200', '350', 'NAVRUZ', 5, 2
exec stp_insert_product'1249', 'POSTER', '30', '140', 'OTHER', 8, 7
exec stp_insert_product'1251', 'BANNER', '60', '200', 'WEDDING', 6, 10
exec stp_insert_product'1250', 'BILLBOARD', '50', '110', 'NAVRUZ', 6, 5
exec stp_insert_product'1245', 'POSTER', '45', '220', 'NEW YEAR', 7, 18
exec stp_insert_product'1235', 'BILLBOARD', '150', '200', 'NAVRUZ', 10, 3
exec stp_insert_product'1237', 'BILLBOARD', '140', '42', 'OTHER', 7, 8
exec stp_insert_product'1249', 'BANNER', '300', '300', 'NEW YEAR', 6, 4
exec stp_insert_product'1251', 'BILLBOARD', '120', '320', 'WOMEN''S DAY', 9, 16
exec stp_insert_product'1237', 'POSTER', '50', '600', 'OTHER', 5, 15
exec stp_insert_product'1249', 'BANNER', '100', '300', 'WEDDING', 3, 18
exec stp_insert_product'1250', 'POSTER', '60', '100', 'NAVRUZ', 8, 4
exec stp_insert_product'1250', 'BANNER', '100', '42', 'OTHER', 7, 19
exec stp_insert_product'1251', 'BILLBOARD', '45', '120', 'TEACHER''S DAY', 4, 4
exec stp_insert_product'1250', 'BANNER', '110', '140', 'NAVRUZ', 7, 1
exec stp_insert_product'1237', 'BANNER', '110', '500', 'NEW YEAR', 3, 10
exec stp_insert_product'1249', 'POSTER', '250', '70', 'NAVRUZ', 5, 17
exec stp_insert_product'1237', 'BANNER', '140', '320', 'OTHER', 10, 5
exec stp_insert_product'1251', 'POSTER', '140', '70', 'OTHER', 6, 19
exec stp_insert_product'1250', 'BANNER', '110', '220', 'WEDDING', 7, 3
exec stp_insert_product'1245', 'BANNER', '35', '150', 'OTHER', 4, 20
exec stp_insert_product'1249', 'BANNER', '100', '350', 'OTHER', 7, 17
exec stp_insert_product'1235', 'BILLBOARD', '50', '110', 'NEW YEAR', 6, 2
exec stp_insert_product'1250', 'BANNER', '60', '400', 'NAVRUZ', 3, 16
exec stp_insert_product'1237', 'BANNER', '300', '300', 'OTHER', 3, 10
exec stp_insert_product'1245', 'BANNER', '250', '110', 'WOMEN''S DAY', 7, 19
exec stp_insert_product'1235', 'POSTER', '35', '400', 'NAVRUZ', 7, 4
exec stp_insert_product'1249', 'BILLBOARD', '35', '70', 'NEW YEAR', 5, 8
exec stp_insert_product'1250', 'BILLBOARD', '100', '150', 'SALE', 3, 6
exec stp_insert_product'1235', 'BANNER', '45', '200', 'NEW YEAR', 6, 5
exec stp_insert_product'1251', 'POSTER', '45', '42', 'NEW YEAR', 5, 8
exec stp_insert_product'1234', 'POSTER', '45', '42', 'TEACHER''S DAY', 6, 18
exec stp_insert_product'1237', 'BILLBOARD', '45', '320', 'NEW YEAR', 8, 1
exec stp_insert_product'1234', 'BANNER', '70', '50', 'NAVRUZ', 8, 16
exec stp_insert_product'1251', 'POSTER', '140', '400', 'WEDDING', 3, 20
exec stp_insert_product'1237', 'BANNER', '30', '42', 'TEACHER''S DAY', 4, 20
exec stp_insert_product'1245', 'BANNER', '150', '600', 'NAVRUZ', 4, 8
exec stp_insert_product'1245', 'POSTER', '70', '120', 'OTHER', 5, 11
exec stp_insert_product'1235', 'BILLBOARD', '30', '45', 'SALE', 8, 16
exec stp_insert_product'1237', 'POSTER', '50', '350', 'OTHER', 10, 3
exec stp_insert_product'1251', 'BANNER', '50', '250', 'OTHER', 3, 4
exec stp_insert_product'1245', 'POSTER', '200', '300', 'NAVRUZ', 10, 4
exec stp_insert_product'1245', 'BANNER', '140', '100', 'SALE', 5, 12
exec stp_insert_product'1251', 'POSTER', '300', '42', 'SALE', 4, 10
exec stp_insert_product'1245', 'POSTER', '140', '220', 'SALE', 3, 15
exec stp_insert_product'1235', 'POSTER', '60', '600', 'NAVRUZ', 4, 7
exec stp_insert_product'1249', 'BILLBOARD', '42', '42', 'TEACHER''S DAY', 9, 8
exec stp_insert_product'1237', 'BANNER', '100', '45', 'WOMEN''S DAY', 10, 5
exec stp_insert_product'1245', 'BANNER', '35', '300', 'WEDDING', 7, 16
exec stp_insert_product'1235', 'BILLBOARD', '120', '220', 'NEW YEAR', 8, 9
exec stp_insert_product'1251', 'BANNER', '110', '42', 'NAVRUZ', 3, 4
exec stp_insert_product'1237', 'BANNER', '140', '120', 'SALE', 8, 14
exec stp_insert_product'1251', 'BILLBOARD', '35', '42', 'NEW YEAR', 6, 13
exec stp_insert_product'1250', 'BILLBOARD', '300', '110', 'WOMEN''S DAY', 3, 19
exec stp_insert_product'1251', 'BILLBOARD', '35', '120', 'TEACHER''S DAY', 5, 19
exec stp_insert_product'1249', 'BILLBOARD', '45', '50', 'WOMEN''S DAY', 7, 19
exec stp_insert_product'1235', 'BANNER', '70', '350', 'TEACHER''S DAY', 7, 13
exec stp_insert_product'1251', 'BANNER', '120', '120', 'OTHER', 8, 12
exec stp_insert_product'1250', 'BILLBOARD', '50', '300', 'SALE', 6, 4
exec stp_insert_product'1250', 'POSTER', '60', '350', 'NEW YEAR', 5, 15
exec stp_insert_product'1249', 'BANNER', '45', '60', 'SALE', 10, 5

/*ORDERS*/
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6574', '1254', 2348, 1, 'CLOSED', '2015/03/07');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6544', '1252', 2347, 4, 'CLOSED', '2015/03/24');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6574', '1255', 2353, 5, 'READY', '2015/03/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6588', '1254', 2352, 4, 'READY', '2015/03/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6544', '1257', 2354, 5, 'READY', '2015/03/01');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6584', '1246', 2347, 5, 'CLOSED', '2015/03/11');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6569', '1248', 2350, 4, 'READY', '2015/03/24');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6586', '1257', 2351, 4, 'READY', '2015/03/05');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6569', '1238', 2349, 1, 'READY', '2015/03/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6614', '1236', 2345, 2, 'CLOSED', '2015/03/21');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6560', '1255', 2351, 4, 'READY', '2015/03/09');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1246', 2346, 2, 'CLOSED', '2015/03/10');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6570', '1254', 2350, 1, 'READY', '2015/03/28');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6552', '1252', 2346, 4, 'CLOSED', '2015/03/06');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6552', '1255', 2352, 4, 'CLOSED', '2015/03/09');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6559', '1252', 2353, 4, 'READY', '2015/03/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1255', 2351, 3, 'CLOSED', '2015/03/23');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6551', '1242', 2353, 1, 'READY', '2015/03/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6570', '1242', 2350, 2, 'CLOSED', '2015/03/20');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6546', '1257', 2349, 1, 'READY', '2015/03/08');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6558', '1254', 2352, 3, 'READY', '2015/03/01');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6550', '1246', 2349, 6, 'CLOSED', '2015/03/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6566', '1255', 2351, 1, 'READY', '2015/03/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6549', '1254', 2351, 1, 'READY', '2015/03/21');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6561', '1246', 2352, 1, 'CLOSED', '2015/03/11');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6561', '1255', 2347, 5, 'READY', '2015/03/24');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6554', '1238', 2347, 5, 'CLOSED', '2015/03/03');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6561', '1252', 2346, 1, 'CLOSED', '2015/03/09');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6554', '1255', 2353, 5, 'CLOSED', '2015/03/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6554', '1254', 2350, 3, 'CLOSED', '2015/03/10');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6547', '1236', 2350, 4, 'CLOSED', '2015/04/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6561', '1257', 2350, 6, 'CLOSED', '2015/04/09');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6550', '1242', 2349, 6, 'CLOSED', '2015/04/19');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6558', '1255', 2345, 6, 'READY', '2015/04/05');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6554', '1254', 2352, 4, 'READY', '2015/04/03');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6547', '1246', 2352, 6, 'CLOSED', '2015/04/27');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6566', '1254', 2346, 3, 'READY', '2015/04/16');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6549', '1246', 2352, 3, 'CLOSED', '2015/04/22');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6566', '1257', 2349, 1, 'READY', '2015/04/15');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6550', '1236', 2354, 5, 'READY', '2015/04/17');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6574', '1252', 2350, 4, 'READY', '2015/04/20');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6574', '1257', 2349, 1, 'READY', '2015/04/21');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6588', '1246', 2350, 5, 'READY', '2015/04/16');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6544', '1236', 2349, 1, 'READY', '2015/04/09');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6588', '1248', 2348, 2, 'CLOSED', '2015/04/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6584', '1252', 2352, 4, 'CLOSED', '2015/04/12');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6569', '1242', 2350, 6, 'READY', '2015/04/04');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6614', '1252', 2349, 2, 'READY', '2015/04/18');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6574', '1252', 2352, 2, 'CLOSED', '2015/04/28');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6579', '1248', 2348, 3, 'READY', '2015/04/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6551', '1236', 2354, 2, 'IN PROGRESS', '2015/04/08');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6562', '1238', 2347, 1, 'IN PROGRESS', '2015/04/16');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6551', '1242', 2350, 3, 'IN PROGRESS', '2015/04/13');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6560', '1246', 2349, 1, 'IN PROGRESS', '2015/04/28');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6552', '1248', 2346, 1, 'IN PROGRESS', '2015/04/08');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6559', '1252', 2347, 4, 'IN PROGRESS', '2015/04/11');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1254', 2349, 1, 'IN PROGRESS', '2015/04/12');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6552', '1255', 2347, 2, 'IN PROGRESS', '2015/04/08');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6559', '1257', 2354, 1, 'IN PROGRESS', '2015/04/26');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1236', 2354, 2, 'IN PROGRESS', '2015/04/19');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6552', '1255', 2345, 3, 'CLOSED', '2015/04/08');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6546', '1246', 2354, 1, 'READY', '2015/04/14');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1236', 2354, 4, 'CLOSED', '2015/04/18');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6557', '1242', 2351, 1, 'READY', '2015/04/02');
insert into ORDERS (PRODUCTNO, SECRETARYNO, CLIENTNO, ORDEREDQUANTITY, ORDERSTATUS, ORDERDATE) values ('6570', '1236', 2354, 3, 'CLOSED', '2015/04/11');

